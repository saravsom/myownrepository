package com.sara.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

import com.sara.rest.dao.Animal;
import com.sara.rest.service.AnimalService;

@Path("/animals")
public class AnimalsResource {

	@Context
	UriInfo uriInfo;
	@Context
	Request request;

	AnimalService animalService;

	public AnimalsResource() {
		animalService = new AnimalService();
	}	


	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public List<Animal> getAnimals() {
		System.out.println("--getAnimals");
		return animalService.getAnimalAsList();
	}

	@GET
	@Produces(MediaType.TEXT_XML)
	public List<Animal> getAnimalsAsHtml() {
		System.out.println("--getAnimalsAsHtml");
		return animalService.getAnimalAsList();
	}

	// URI: /rest/animals/count
	@GET
	@Path("count")
	@Produces(MediaType.TEXT_PLAIN)
	public String getCount() {
		System.out.println("--getCount");
		return String.valueOf(animalService.getAnimalsCount());
	}

	@POST
	@Produces(MediaType.TEXT_HTML)
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void createAnimal(@FormParam("id") String id,
			@FormParam("animalname") String name,
			@FormParam("animaltype") String type,
			@Context HttpServletResponse servletResponse) throws IOException {
		System.out.println("--createAnimal");
		List<String> defTests = new ArrayList<String>();
		defTests.add("Default1");
		defTests.add("Default2");
		Animal animal = new Animal(id, name, type, defTests);
		animalService.createAnimal(animal);
		servletResponse.sendRedirect("./animals/");
	}

	@Path("{animal}")
	public AnimalResource getAnimal(@PathParam("animal") String id) {
		System.out.println("--"+id+","+request+","+uriInfo);
		return new AnimalResource(uriInfo, request, id);
		
	}

}