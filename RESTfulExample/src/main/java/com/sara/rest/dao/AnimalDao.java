package com.sara.rest.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum AnimalDao {
	instance;

	private Map<String, Animal> animals = new HashMap<String, Animal>();

	private AnimalDao() {
		List<String> tests = new ArrayList<String>();
		tests.add("Sara");
		tests.add("vanan");
		List<String> tests1 = new ArrayList<String>();
		tests1.add("Sundar");
		tests1.add("Murthy");
		//pumping-in some default data
		Animal animal = new Animal("1", "Lion", "Wild",tests);
		
		animals.put("1", animal);
		animal = new Animal("2", "Crocodile", "Wild",tests1);
		animals.put("2", animal);

	}

	public Map<String, Animal> getAnimals() {
		return animals;
	}

}