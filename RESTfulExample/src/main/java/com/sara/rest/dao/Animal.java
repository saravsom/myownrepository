package com.sara.rest.dao;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlList;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Animal {
	private String id;
	private String name;
	private String type;
	@XmlElement
	@XmlList
	private List<String> tests = new ArrayList<String>();

	public Animal() {
	}

	public Animal(String id, String name, String type, List<String> tests) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
		this.tests.addAll(tests);
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
